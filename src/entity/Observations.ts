import { Entity, PrimaryGeneratedColumn, Column, Timestamp, ManyToOne, OneToOne, JoinColumn, PrimaryColumn } from 'typeorm/browser'
import { Forms } from './Forms'
import { Places } from './Places'
import { Status } from './Status'
import { type } from 'os';


@Entity('Observations')
export class Observations {

    @PrimaryGeneratedColumn()
    id: number

    @Column({ type: 'varchar' })
    date: string

    @Column({ type: "varchar" })
    time: string



    @ManyToOne(type => Places, place => place.observations)

    place: Places[]

    @ManyToOne(type => Forms, form => form.observations)

    form: Forms[]

    @ManyToOne(type => Status, status => status.observations)

    status: Status[];

    @Column("varchar", {nullable: true})
    copypattern: string


}