import{ Entity, PrimaryGeneratedColumn, Column, PrimaryColumn, OneToMany } from 'typeorm/browser'
import { Observations } from './Observations';

@Entity('Status')
export  class Status{
    
   @PrimaryGeneratedColumn()
    id: number

@Column("varchar")
    name: string

    @OneToMany (type => Observations, observation => observation.status)
    observations: Observations[]
}