
import { Entity, PrimaryGeneratedColumn, Column, OneToMany, Table, EntitySchema, OneToOne } from 'typeorm/browser'
import { Observations } from './Observations'
import { isPattern } from '@babel/types';


@Entity('Forms')
export class Forms {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar")
    book: string;


    @OneToMany(type => Observations, observation => observation.form)
    observations: Observations[]

    @Column('varchar',{nullable: true})
    pattern: string;
}
