
import{ Entity, PrimaryGeneratedColumn, Column, PrimaryColumn, Table, EntitySchema } from 'typeorm/browser'




@Entity('Images')
export  class Images{
   @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar")
    base64: string;

    @Column("varchar")
    width: string

    @Column("varchar")
    height: string

    @Column("varchar")
    uri: string
    
    @Column('varchar', {nullable: true})
    exif: string

    @Column("int")
    pictureOrientation: number

    @Column("int")
    deviceOrientation: number

    @Column('int')
    currentLongitude: number

    @Column('int')
    currentLatitude: number


}