import{ Entity, PrimaryGeneratedColumn, Column, Timestamp, ManyToOne, OneToOne, JoinColumn, PrimaryColumn } from 'typeorm/browser'
import {Forms}  from './Forms'
import {Places}  from './Places'
import { identifier } from '@babel/types';

@Entity('Settings')
export class Settings{
    @PrimaryGeneratedColumn()
    id: number

    @OneToOne(type => Forms)
    @JoinColumn({name: "formId", referencedColumnName: "id"})
    form: Forms[]
//formID
    @OneToOne(type => Places)
    @JoinColumn({name: "placeId", referencedColumnName: "id"})
    place: Places[]
//placeID
    @Column( {type: Boolean})
    snowShooting: null 
}