import { Entity, PrimaryGeneratedColumn, Column, OneToMany, Table, EntitySchema, OneToOne } from 'typeorm/browser'

@Entity('Dictionaries')
export class Dictionaries {
    @PrimaryGeneratedColumn()
    id: number

    @Column('varchar')
    dictionary: string

}