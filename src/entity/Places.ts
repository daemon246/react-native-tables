import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm/browser'
import {Observations} from './Observations'

@Entity('Places')
export class Places {

    @PrimaryGeneratedColumn()
    id: number

    @Column("varchar")
    name: string

    @Column("varchar")
    shortName: string

    @OneToMany(type => Observations, observation => observation.place)
    observations: Observations[]
}