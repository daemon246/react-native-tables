import { createConnection, getConnection, ConnectionManager, getRepository, EntitySchema, createQueryBuilder } from 'typeorm/browser/'

//Показать таблицу целиком 
/**
 * 
 * @param myTable - Таблица из БД
 */
let  read =  (myTable) =>  {
    // const table = 
    return getConnection()
        .createQueryBuilder()
        .select()
        .from(myTable, '')
        .execute()

    // this.setState({ [myTable]: table })
}



//Создать элемент в таблице
/**
 * 
 * @param myTable - Таблица из БД
 * @param obj - Новый объект в таблице
 */
let create = async (myTable, obj) => {

    await getConnection()
        .createQueryBuilder()
        .insert()
        .into(myTable)
        .values([
            obj
        ])
        .execute()
}
//Найти элемент таблицы по ID
/**
 * 
 * @param myTable - Таблица из БД
 * @param myID - ID строки, которую нужно спарсить
 */
let readID = async (myTable, myID) => {

    const table = await getConnection()
        .createQueryBuilder()
        .select()
        .from(myTable, '')
        .where("id = :id", { id: myID })
        .execute()
    this.setState({ table: table })
}

//Удалить элемент в таблице по ID
/**
 * 
 * @param myTable - Таблица из БД
 * @param myID - ID строки, которую нужно удалить
 */
let deleter = async (myTable, myID) => {

     await getConnection()
        .createQueryBuilder()
        .delete()
        .from(myTable) //table
        .where("id = :id", { id: myID }) //id
        .execute()

}

//Удалить данные из таблицы
/**
 * 
 * @param myTable - Таблица из БД
 */
let deleteAll = async (myTable) => {

    await getConnection()
        .createQueryBuilder()
        .delete()
        .from(myTable)
        .execute()

}

//Обновить данные в таблице по ID. (ID, новый объект)
/**
 * 
 * @param myTable - Таблица из БД
 * @param oldObj - ID данных, которые нужно изменить
 * @param newObj - новые данные
 */
let update = (myTable, oldObj, newObj) => {
    return getConnection()
        .createQueryBuilder()
        .update(myTable)
        .set(newObj)
        .where("id = :id", { id: oldObj })
        .execute()

}

export default (read, update, deleter, deleteAll, readID, create)