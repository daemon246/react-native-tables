/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * 
 * Generated with the TypeScript template
 * https://github.com/emin93/react-native-template-typescript
 * 
 * @format
 */

// [
//   "@babel/plugin-proposal-decorators",
//   {
//       "legacy": true
//   }
// ], 
// [
//   "@babel/plugin-proposal-class-properties",
//   {
//       "loose": true
//   }
// ]
<script src="http://localhost:8097"></script>

import "reflect-metadata"

import React, { Component } from 'react';
import { TouchableOpacity, BackHandler, StyleSheet, Text, View, Button, Alert, CameraRoll, Image } from 'react-native';
import { WebView } from 'react-native-webview';
import { Forms } from './src/entity/Forms'
import { Observations } from './src/entity/Observations'
import { Places } from './src/entity/Places'
import { Status } from './src/entity/Status'
import { Images } from './src/entity/Images'
import { Settings } from './src/entity/Settings'
import { Dictionaries } from './src/entity/Dictionaries'
import { createConnection, getConnection, ConnectionManager, getRepository, EntitySchema, createQueryBuilder } from 'typeorm/browser/'
import { RNCamera } from 'react-native-camera'











//Подключение JSON файлов

var tm8 = require('./android/app/src/main/assets/files/TM-8.json')
var kg1m = require('./android/app/src/main/assets/files/KG-1M.json')
var kg1mo = require('./android/app/src/main/assets/files/KG-1MO.json')
var kg3m = require('./android/app/src/main/assets/files/KG-3M.json')
var kg10 = require('./android/app/src/main/assets/files/KG-10.json')
var kg64 = require('./android/app/src/main/assets/files/KG-64.json')
var dictionary = require('./android/app/src/main/assets/files/dictionary.json')

// import console = require("console");








interface Props { }
export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isMainMenu: true, //Главный экран
      isCreatePhoto: false, //Отображение сделанной фотографии
      data: {},            // Данные о фотографии
      isCameraVisible: false, //Отображение камеры
      path: {}, //Хранение абсолютного пути сделанной фотографии
      height: null,
      width: null,
      currentLongitude: null,//Initial Longitude
      currentLatitude: null,//Initial Latitude
    }
    this.webView = React.createRef()
  }

  //Подключение к БД
  dbconnect() {
    return createConnection({
      type: 'react-native',
      database: 'database',
      synchronize: true,
      location: 'default',
      logging: ['error', 'query', 'schema'],
      entities: [
        Observations,
        Places,
        Forms,
        Status,
        Images,
        Settings,
        Dictionaries
      ]
    });
  }

  //Функция чтения строки из таблицы по id
  copyPattern = (id) => {
    return getConnection()
      .createQueryBuilder()
      .select()
      .from('Forms', '')
      .where("id = :id", { id: id })
      .execute()
  }
  //Функция чтения pattern из таблицы Forms по id с использованием функции copyPattern
  workpattern = async (id) => {
    let res = await this.copyPattern(id)

    return res[0].pattern
  }




  //Показать таблицу целиком 
  /**
   * 
   * @param myTable - Таблица из БД
   */
  read = (myTable) => {
    // const table = 
    return getConnection()
      .createQueryBuilder()
      .select()
      .from(myTable, '')
      .execute()

    // this.setState({ [myTable]: table })
  }



  //Создать элемент в таблице
  /**
   * 
   * @param myTable - Таблица из БД
   * @param obj - Новый объект в таблице
   */
  create = async (myTable, obj) => {

    await getConnection()
      .createQueryBuilder()
      .insert()
      .into(myTable)
      .values([
        obj
      ])
      .execute()
  }
  //Найти элемент таблицы по ID
  /**
   * 
   * @param myTable - Таблица из БД
   * @param myID - ID строки, которую нужно спарсить
   */
  readID = async (myTable, myID) => {

    const table = await getConnection()
      .createQueryBuilder()
      .select()
      .from(myTable, '')
      .where("id = :id", { id: myID })
      .execute()
    this.setState({ table: table })
  }

  //Удалить элемент в таблице по ID
  /**
   * 
   * @param myTable - Таблица из БД
   * @param myID - ID строки, которую нужно удалить
   */
  delete = async (myTable, myID) => {

    await getConnection()
      .createQueryBuilder()
      .delete()
      .from(myTable) //table
      .where("id = :id", { id: myID }) //id
      .execute()

  }

  //Удалить данные из таблицы
  /**
   * 
   * @param myTable - Таблица из БД
   */
  deleteAll = async (myTable) => {

    await getConnection()
      .createQueryBuilder()
      .delete()
      .from(myTable)
      .execute()

  }

  //Обновить данные в таблице по ID. (ID, новый объект)
  /**
   * 
   * @param myTable - Таблица из БД
   * @param oldObj - ID данных, которые нужно изменить
   * @param newObj - новые данные
   */
  update = (myTable, oldObj, newObj) => {
    return getConnection()
      .createQueryBuilder()
      .update(myTable)
      .set(newObj)
      .where("id = :id", { id: oldObj })
      .execute()

  }

  componentDidMount() {
    //Подключение к БД при запуске
    this.dbconnect();
    //обработка нажатие кнопки "Назад"
    BackHandler.addEventListener('hardwareBackPress', this.goBack)

  }



  //Закрытие камеры через state
  goBack = async () => {
    this.setState({ isCameraVisible: false });
    this.setState({ isCreatePhoto: false, isMainMenu: true })

  }

  //Слушатель событий, по которым выполняются функции для работы с БД
  onMessage = (event) => {
    const { data } = event.nativeEvent;

    let input = JSON.parse(data)


    switch (input.type) {

      //Чтение и отправка всех данных с БД
      case 'ReadAll':
        Promise.all([
          this.read("Observations"),
          this.read("Settings"),
          this.read("Status"),
          this.read("Forms"),
          this.read("Places"),
          this.read("Images"),
          this.read("Dictionaries")
        ]).then(result => {
          let payload = {
            observations: result[0],
            settings: result[1],
            status: result[2],
            forms: result[3],
            places: result[4],
            images: result[5],
            dictionaries: result[6]
          }

          let script = `window.emitter.emit('read','${JSON.stringify(payload)}')`

          this.webView.injectJavaScript(script)
        })
        break;
      //Чтение какой-то отдельной таблицы из БД
      case 'ReadTable':
        Promise.all([
          this.read(input.table[0].toUpperCase() + input.table.slice(1)), //делаем 1 букву заглавной, т.к. input.table все буквы маленькие
        ]).then(result => {
          let payload = {
            [input.table]: result[0],
          }
          let script = `window.emitter.emit('read','${JSON.stringify(payload)}')`
          this.webView.injectJavaScript(script)
        })
        break;

      //Отправка всех данных из таблицы Image
      case 'ReadImg':
        this.read("Images").then(result => {

          let payload = {
            images: result
          }
          let script = `window.emitter.emit('readImg', ${JSON.stringify(payload)})`
          this.webView.injectJavaScript(script)
        }
        )
        break;
      //Тестовое внесение данных в таблицу
      case 'CreateObservation':
        Promise.all([
          this.create("Observations", { date: input.date, time: input.time, place: input.place, form: input.form, status: input.status, copypattern: input.copypattern })
        ])
        break;
      //Изменение настроек 
      case 'UpdateSettings':
        Promise.all([
          this.update("Settings", 1, { place: input.selectedPlace, snowShooting: input.isSnowShooting })
        ])

        break;
      //Изменение статуса отправки
      case 'ChangeStatus':
        Promise.all([
          this.update("Observations", input.id, { status: input.status, copypattern: input.copypattern })
        ])
        break;
      case 'ChangePattern':
        Promise.all([
          this.update("Observations", input.id, { copypattern: input.copypattern })
        ]).then(() => Alert.alert('ChangePattern'))

        break;
      case 'ChangeProp':
        // [input.prop] = 'coppypattern' or 'status'
        Promise.all([
          this.update("Observations", input.id, { [input.prop]: input[input.prop] }) //let cz = {[arr.prop]: arr[arr.prop]}
        ])
        break;

      //Создание начальных данных в БД при первоначальном запуске, вызывается только один раз
      case 'CreateTable':
        Promise.all([
          this.create("Places", { name: "Речной гидрологический пост (ГП)", shortName: "Река" }),
          this.create("Places", { name: "Озерной гидрологический пост (ГП)", shortName: "Озеро" }),

          this.create("Status", { name: "Черновик" }),
          this.create("Status", { name: "Ошибка" }),
          this.create("Status", { name: "Отправлено" }),
          this.create("Status", { name: "Отправка.." }),

          this.create("Forms", { book: "КГ-1М", pattern: encodeURI(JSON.stringify(kg1m)) }),
          this.create("Forms", { book: "КГ-1МО", pattern: encodeURI(JSON.stringify(kg1mo)) }),
          this.create("Forms", { book: "КГ-3М", pattern: encodeURI(JSON.stringify(kg3m)) }),
          this.create("Forms", { book: "КГ-10", pattern: encodeURI(JSON.stringify(kg10)) }),
          this.create("Forms", { book: "КГ-64", pattern: encodeURI(JSON.stringify(kg64)) }),
          this.create("Forms", { book: "ТМ-8", pattern: encodeURI(JSON.stringify(tm8)) }),

          this.create('Dictionaries', { dictionary: encodeURI(JSON.stringify(dictionary)) }),

          this.create("Settings", { form: 4, place: 1, snowShooting: true }),
        ])
        break;

      //Чтение строки из таблицы по id
      case 'ReadID':
        this.readID("Forms", 4)
          .then(async () => {
            var js = JSON.stringify(this.state.table)
            var script = `setResult(` + JSON.stringify(js) + `)`
            this.webView.injectJavaScript(script)
            Alert.alert('Done')
          })
        break;
      //Вызов функции для удаления строки из Observations по id
      case 'DeleteObservationsID':
        this.delete("Observations", input.id)
          .then(async () => {
            var result = 'Deleted id'
            Alert.alert(result)
          })
        break;
      //Вызов функции для удаления всех данных из таблицы
      case 'DeleteAll':
        this.deleteAll("Images")
          .then(async () => {
            var result = 'Table is clean'
            Alert.alert(result)
          })
        break;
      //Вызов функции для показа камеры
      case 'СreatePhoto':
        this.showCameraView()
        // console.log('Work')
        break;

      case 'LastImg':
        this.lastImg().then(result => {
          console.log('result ==== ', result)
          let script = `window.emitter.emit("readImg", ${JSON.stringify(result)})`
          this.webView.injectJavaScript(script)
        })


    }

  }

  lastImg = async () => {
    return await getConnection()
      .createQueryBuilder()
      .select()
      .from("Images", '')
      .orderBy({ id: 'DESC' })
      .getRawMany()
  }

  //Функция для создание фото и получения геолокации пользователя, привязана к браузерной кнопке, затем идет запись в БД о свойствах данной фотографии
  takePicture = async () => {
    navigator.geolocation.getCurrentPosition(     //Определяем геолокацию пользователя и сохраняем ее в state
      (position) => {
        const currentLongitude = position.coords.longitude;
        const currentLatitude = position.coords.latitude;
        this.setState({ currentLongitude: currentLongitude, currentLatitude: currentLatitude })
      }
    )
    if (this.camera) {
      const options = { quality: 0.5, base64: true, exif: true };
      const data = await this.camera.takePictureAsync(options)
      this.setState({ data: data, path: data.uri, width: data.width, height: data.height, isCameraVisible: false, isCreatePhoto: true })


    }

  };
  //Функция сохранения свойств фотографии в БД
  saveImg = async (myTable, obj) => {
    await getConnection()
      .createQueryBuilder()
      .insert()
      .into(myTable)
      .values([
        obj
      ])
      .execute()

  }

  //Функция для добавления фото в БД по нажатию кнопки, затем  выводится главный экран 
  //Данные для сохраниения в БД берутся из стейта data
  confirmPhoto = async () => {
    await this.saveImg('Images', {
      base64: this.state.data.base64,
      width: this.state.data.width,
      height: this.state.data.height,
      uri: this.state.data.uri,
      exif: encodeURI(JSON.stringify(this.state.data.exif)),
      pictureOrientation: this.state.data.pictureOrientation,
      deviceOrientation: this.state.data.deviceOrientation,
      currentLatitude: this.state.currentLatitude,
      currentLongitude: this.state.currentLongitude

    }).then(async () => {

      await this.read("Images").then(result => {

        let script = `window.emitter.emit('readImg','${JSON.stringify(result)}')`
        this.webView.injectJavaScript(script)
        this.setState({ isCreatePhoto: false, isMainMenu: true })
      })
      // let script = `window.emitter.emit("insertImg", ${JSON.stringify(this.state.data.base64)})`
      // this.webView.injectJavaScript(script)
      //После сохранения фото показывается галвный экран

    })
  }


  //Функция для отмены сделанной фотографии, затем выводится камера
  canclePhoto = async () => {
    this.setState({ isCreatePhoto: false, isCameraVisible: true })
  }

  //Установка камеры в позицию true
  showCameraView = () => {

    this.setState({ isCameraVisible: true });

  }
  oneMore = async () => {
    await this.saveImg('Images', {
      base64: this.state.data.base64,
      width: this.state.data.width,
      height: this.state.data.height,
      uri: this.state.data.uri,
      exif: encodeURI(JSON.stringify(this.state.data.exif)),
      pictureOrientation: this.state.data.pictureOrientation,
      deviceOrientation: this.state.data.deviceOrientation,
      currentLatitude: this.state.currentLatitude,
      currentLongitude: this.state.currentLongitude

    }).then(async () => {
      this.setState({ isCreatePhoto: false, isCameraVisible: true })
    })

  }

  backFunc = () => {
    this.setState({ isMainMenu: true, isCreatePhoto: false, isCameraVisible: false })
  }

  //Отображение WebView и остальных контейнеров
  render = () => {
    const { isCameraVisible, isCreatePhoto, isMainMenu } = this.state; // Задаем стейты для работы с контейнерами
    return (
      <View style={{ height: "100%" }} >
        {isMainMenu &&               //Отображение главного экрана, если isMainMenu установлена в позицию true (по дефолту)
          <WebView
            ref={r => (this.webView = r)}
            onMessage={this.onMessage}
            scalesPageToFit={false}
            javaScriptEnabled={true}
            source={{ uri: 'http://192.168.0.101:3000/' }}
            startInLoadingState={true}

          />
        }

        {
          isCameraVisible &&         // Открытие камеры, если isCameraVisible в позиции true (по дефолту false)

          <View style={{ height: "100%", justifyContent: 'center' }}>
            <View style={{ flex: 1, height: "90%", justifyContent: 'center' }}>
              <RNCamera ref={ref => {
                this.camera = ref;
              }}

                style={styles.preview}
                type={RNCamera.Constants.Type.back}
                flashMode={RNCamera.Constants.FlashMode.off}
                focusDepth={0.5}

              >

              </RNCamera>
            </View>
            <View style={{ height: "10%", justifyContent: 'center', flexDirection: 'row', backgroundColor: 'black' }}>
              <TouchableOpacity onPress={this.takePicture.bind(this)} ><Image style={{ width: 80, height: 80 }} source={require('./src/imgs/camera.png')} /></TouchableOpacity>
              <TouchableOpacity onPress={this.backFunc.bind(this)} ><Image style={{ width: 80, height: 80 }} source={require('./src/imgs/back.jpg')} /></TouchableOpacity>
            </View>

          </View>
        }
        {
          isCreatePhoto &&         //Показ сделанной фотографии с кнопками подтверждения и отмены (по дефолту false)
          < View style={{ height: "100%", justifyContent: 'center', alignItems: 'center' }}>
            <Image source={{ uri: this.state.path }} style={{ height: this.state.height / 2, width: this.state.width / 2 }} />
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
              <TouchableOpacity onPress={this.confirmPhoto.bind(this)} ><Image style={{ width: 70, height: 70 }} source={require('./src/imgs/confirm.jpg')} /></TouchableOpacity>
              <TouchableOpacity onPress={this.canclePhoto.bind(this)}><Image style={{ width: 70, height: 70 }} source={require('./src/imgs/cancle.jpg')} /></TouchableOpacity>
              <TouchableOpacity onPress={this.oneMore.bind(this)}><Image style={{ width: 70, height: 70 }} source={require('./src/imgs/add.png')} /></TouchableOpacity>
              <TouchableOpacity onPress={this.backFunc.bind(this)}><Image style={{ width: 70, height: 70 }} source={require('./src/imgs/back.png')} /></TouchableOpacity>

            </View>
          </View >

        }
      </View >

    );
  }

}
const styles = StyleSheet.create({
  container: {
    height: 350,
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',

  },
  buttonBack: {
    width: 70,
    height: 70,
    borderRadius: 35,
    borderWidth: 5,
    borderColor: 'white',
    marginBottom: 15,

  },
  buttonNew: {
    width: 70,
    height: 70,
    borderRadius: 35,
    borderWidth: 5,
    borderColor: 'blue',
    marginBottom: 15,

  },
  capture: {
    width: 70,
    height: 70,
    borderRadius: 35,
    borderWidth: 5,
    borderColor: 'red',
    marginBottom: 15,
  },

  buttonOk: {
    width: 70,
    height: 70,
    borderRadius: 35,
    borderWidth: 5,
    borderColor: 'green',
    marginBottom: 15,

  },
  buttonCancle: {
    width: 70,
    height: 70,
    borderRadius: 35,
    borderWidth: 5,
    borderColor: 'red',
    marginBottom: 15,

  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
