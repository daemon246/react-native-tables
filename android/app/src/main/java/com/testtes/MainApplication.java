package com.testtes;
import com.wix.RNCameraKit.RNCameraKitPackage;
import android.app.Application;
import android.webkit.WebView;
import com.facebook.react.ReactApplication;
//import com.benwixen.rnfilesystem.RNFileSystemPackage;
//import com.rnfs.RNFSPackage;
import org.reactnative.camera.RNCameraPackage;
import com.github.alinz.reactnativewebviewbridge.WebViewBridgePackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import java.util.Arrays;
import java.util.List;
import org.pgsqlite.SQLitePluginPackage;
public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
              new RNCWebViewPackage(),
              new SQLitePluginPackage(),
              new MainReactPackage(),
            //new RNFileSystemPackage(),
              new RNCameraKitPackage(),
            new RNCameraPackage(),
              new WebViewBridgePackage()


      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    WebView.setWebContentsDebuggingEnabled(true);
    SoLoader.init(this, /* native exopackage */ false);
  }
}
